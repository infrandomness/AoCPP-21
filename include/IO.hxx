#ifndef AOC_IO_HXX
#define AOC_IO_HXX

#include <iosfwd>
#include <string>
#include <vector>

namespace IO
{
    /// Reads a file into an std::vector<int> by newlines
    ///
    /// Usage:
    /// \code
    ///     char *str[] = { "bla", "bla", "bla", "bla" }
    ///     cstrArrayToVector(4, str);
    /// \endcode
    ///
    /// \param length the number of char * into the char **
    /// \param array the char ** containing the elements
    ///
    /// \returns vector<std::string> containing all of the char * as std::string
    std::vector<int> readFileToIntVector(std::ifstream &file);

    // Creates an input file stream from a std::string
    // https://cplusplus.github.io/LWG/issue3430
    std::ifstream openFile(const std::string& path);
}

#endif // AOC_IO_HXX
