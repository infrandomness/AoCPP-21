#ifndef AOC_REPORTREPAIR_HXX
#define AOC_REPORTREPAIR_HXX

#include "Day.hxx"
#include "IO.hxx"

#include <fstream>
#include <iostream>
#include <span>
#include <string>

namespace partOne
{
    static int calculateValue(std::vector<int> numbers);
}; // namespace partOne

namespace partTwo
{
    static int calculateValue(std::vector<int> numbers);
}; // namespace partTwo

class ReportRepair final : public Day {

  public:
    ReportRepair() = default;

    void run(std::span<std::string_view> args) override;
};

#endif // AOC_REPORTREPAIR_HXX