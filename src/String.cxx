#include "String.hxx"

#include <vector>

std::vector<std::string_view> String::cstrArrayToVector(int length,
                                                        char **array) {
    std::vector<std::string_view> result;
    result.reserve(length);
    for (int i = 0; i < length; i++) {
        result.emplace_back(std::string_view(array[i]));
    }
    return result;
}